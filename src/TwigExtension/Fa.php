<?php

namespace Drupal\twig_fa\TwigExtension;

use Drupal\Core\Template\Attribute;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Provides the fa function for Twig templates.
 *
 * @package Drupal\twig_fa\TwigExtension
 */
class Fa extends AbstractExtension {

  /**
   * The default attributes.
   *
   * @var string[]
   */
  protected $defaultAttributes = [
    'fill' => 'currentColor',
    'style' => 'height: 1em;',
  ];

  /**
   * Constructs a new Fa object.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('fa', [$this, 'fa']),
    ];
  }

  /**
   * Outputs a Font Awesome inline SVG.
   *
   * @param string $style
   *   The icon style.
   * @param string $name
   *   The icon name.
   * @param array $attributes
   *   The array of attributes to add to the <svg> tag.
   *
   * @return array
   *   The inline SVG or void if the icon does not exist.
   */
  public function fa($style, $name, $attributes = []) {
    if (!is_string($style) || !is_string($name)) {
      return [];
    }
    $style = $this->normalize($style);
    $name = $this->normalize($name);
    $svg_path = DRUPAL_ROOT . '/libraries/fontawesome/svgs/' . $style . '/' . $name . '.svg';
    if (!file_exists($svg_path)) {
      return [];
    }
    $svg = file_get_contents($svg_path);
    if (isset($attributes['style'])) {
      $attributes['style'] = $this->defaultAttributes['style'] . $attributes['style'];
    }
    $attributes = new Attribute(array_merge($this->defaultAttributes, $attributes));
    $svg = substr_replace($svg, $attributes, 4, 0);
    return [
      '#type' => 'inline_template',
      '#template' => $svg,
    ];
  }

  /**
   * Normalizes an icon name or style.
   *
   * @param string $value
   *   The string to normalize.
   *
   * @return string
   *   The normalized string.
   */
  protected function normalize(string $value) {
    return preg_replace('/[^a-z0-9-]+/', '', strtolower($value));
  }

}
