# Twig Font Awesome

The Twig Font Awesome module provides a simple Twig function for inserting Font Awesome icons as inline SVGs.


## Requirements

This module requires the `svgs` folder from the Font Awesome library to be placed at `/libraries/fontawesome/svgs`.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is needed.


## Maintainers

- Parkle Lee - [phparkle](https://www.drupal.org/u/phparkle)
